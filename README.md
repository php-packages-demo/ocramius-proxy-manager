# ocramius-proxy-manager

A library providing utilities to generate, instantiate and generally operate with Object Proxies. https://packagist.org/packages/ocramius/proxy-manager